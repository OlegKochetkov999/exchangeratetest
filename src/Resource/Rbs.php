<?php

namespace OK\ExchangeRate\Resource;

use OK\ExchangeRate\Client;
use OK\ExchangeRate\Entity\ParameterBag;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

/**
 * @author Oleg Kochetkov <oleg.kochetkov999@yandex.ru>
 */
class Rbs extends AbstractResource
{
    /**
     * @var string
     */
    protected $url = 'https://cash.rbc.ru/cash/json/converter_currency_rate';

    /**
     * @var array
     */
    protected $params = [
        'currency_from' => '',
        'currency_to' => '',
        'source' => 'cbrf',
        'sum' => '1',
        'date' => ''
    ];
    
    /**
     * @param ParameterBag $params
     * @return float|null
     */
    public function get(ParameterBag $params): ?float
    {
        $this->mapping($params);
        $rawData = Client::get($this->generateUrl());
        $data = (new JsonEncoder)->decode($rawData, JsonEncoder::FORMAT);
        
        return $data['data']['sum_result'] ?? null;
    }
    
    /**
     * @param ParameterBag $params
     */
    protected function mapping(ParameterBag $params)
    {
        $this->params['date'] = $params->getDate()->format('Y-m-d');
        $this->params['currency_from'] = $params->getCurrencyFrom();
        $this->params['currency_to'] = $params->getCurrencyTo();
    }
}
