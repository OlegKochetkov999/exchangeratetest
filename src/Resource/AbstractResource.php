<?php

namespace OK\ExchangeRate\Resource;

use OK\ExchangeRate\Entity\ParameterBag;

/**
 * @author Oleg Kochetkov <oleg.kochetkov999@yandex.ru>
 */
abstract class AbstractResource
{
    /**
     * @var string 
     */
    protected $url = '';
    
    /**
     * @var AbstractResources[] 
     */
    protected $params = [];

    /**
     * @param ParameterBag $params
     * @return null|float
     */
    abstract public function get(ParameterBag $params): ?float;
    
    /**
     * @param ParameterBag $params
     * @return void
     */
    abstract protected function mapping(ParameterBag $params);
    
    /**
     * @return string
     */
    protected function generateUrl(): string
    {
        $query = '?';
        foreach ($this->params as $key => $value) {
            if (!empty($value)) {
                $query .= "$key=$value&";
            }
        }
        
        return $this->url . substr($query, 0, strlen($query) - 1);
    }
    
}
