<?php

namespace OK\ExchangeRate\Resource;

use OK\ExchangeRate\Client;
use OK\ExchangeRate\Entity\ParameterBag;
use Symfony\Component\Serializer\Encoder\XmlEncoder;

/**
 * @author Oleg Kochetkov <oleg.kochetkov999@yandex.ru>
 */
class Cbr extends AbstractResource
{
    /**
     * @var string
     */
    protected $url = 'http://www.cbr.ru/scripts/XML_daily.asp';
    
    /**
     * @var array
     */
    protected $params = [
        'date_req' => ''
    ];
    
    /**
     * @param ParameterBag $params
     * @return float|null
     */
    public function get(ParameterBag $params): ?float
    {
        $this->mapping($params);
        $rawData = Client::get($this->generateUrl());
        $data = (new XmlEncoder)->decode($rawData, XmlEncoder::FORMAT);
        $value = null;
        
        if (!isset($data['Valute'])) {
            return null;
        }
        
        foreach ($data['Valute'] as $rate) {
            if (isset($rate['CharCode']) && $rate['CharCode'] === $params->getCurrencyFrom()) {
                $value = (float)$rate['Value'];
                break;
            }
        }
        
        return $value;
    }
    
    /**
     * @param ParameterBag $params
     */
    protected function mapping(ParameterBag $params)
    {
        $this->params['date_req'] = $params->getDate()->format('d/m/Y');
    }
}
