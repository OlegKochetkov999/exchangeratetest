<?php

namespace OK\ExchangeRate;

use OK\ExchangeRate\Entity\ParameterBag;
use OK\ExchangeRate\Exception\ExchangeException;

/**
 * @author Oleg Kochetkov <oleg.kochetkov999@yandex.ru>
 */
class ExchangeRate
{
    /**
     * @var ParameterBag
     */
    private $settings;
    
    /**
     * @var AbstractResource[]
     */
    private $resources = [];
    
    /**
     * 
     * @param array|AbstractResources[] $resources
     * @param ParameterBag|null $settings
     */
    public function __construct(array $resources = [], ParameterBag $settings = null)
    {
        $this->settings = $settings ?? new ParameterBag();
        $this->resources = $resources;
    }
    
    /**
     * @return float
     * @throws ExchangeException
     */
    public function get(): float
    {
        $sum = 0.0;
        $count = 0;

        foreach ($this->resources as $resource) {
            $value = $resource->get($this->settings);
            
            if ($value === null) {
                throw new ExchangeException('Service ' . get_class($resource) . ' returns invalid result');
            }
            
            $sum += (float)$value;
            $count++;
        }
        
        return $count ? (float)$sum/$count : $sum;
    }
    
    /**
     * @return ParameterBag
     */
    public function getSettings(): ParameterBag
    {
        return $this->settings;
    }
}
