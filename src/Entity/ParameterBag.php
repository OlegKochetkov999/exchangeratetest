<?php

namespace OK\ExchangeRate\Entity;

/**
 * @author Oleg Kochetkov <oleg.kochetkov999@yandex.ru>
 */
class ParameterBag
{
    /**
     * @var string
     */
    private $currencyFrom;
    
    /**
     * @var string
     */
    private $currencyTo;
    
    /**
     * @var \DateTime
     */
    private $date;
    
    /**
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        foreach ($data as $key => $value) {
            if (property_exists(ParameterBag::class, $key)) {
                $this->$key = $value;
            }
        }
    }
    
    /**
     * @return string
     */
    public function getCurrencyFrom(): string
    {
        return $this->currencyFrom;
    }
    
    /**
     * 
     * @param string $currency
     * @return ParameterBag
     */
    public function setCurrencyFrom(string $currency): ParameterBag
    {
        $this->currencyFrom = $currency;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getCurrencyTo(): string
    {
        return $this->currencyTo;
    }
    
    /**
     * @param string $currency
     * @return ParameterBag
     */
    public function setCurrencyTo(string $currency): ParameterBag
    {
        $this->currencyTo = $currency;

        return $this;
    }
    
    /**
     * @return \DateTime
     */
    public function getDate(): \DateTime
    {
        return $this->date;
    }
    
    /**
     * 
     * @return ParameterBag
     */
    public function setDate(\DateTime $date): ParameterBag
    {
        $this->date = $date;
        
        return $this;
    }
}