<?php

namespace OK\ExchangeRate\Exception;

/**
 * @author Oleg Kochetkov <oleg.kochetkov999@yandex.ru>
 */
class ExchangeException extends \Exception
{
}
