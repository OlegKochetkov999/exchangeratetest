<?php

namespace OK\ExchangeRate;

use OK\ExchangeRate\Exception\ClientException;

/**
 * @author Oleg Kochetkov <oleg.kochetkov999@yandex.ru>
 */
class Client
{
    /**
     * @param string $url
     * @return string
     * @throws ClientException
     */
    public static function get(string $url): string
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

        $data = curl_exec($ch);
        curl_close($ch);

        if ($data === false) {
            throw new ClientException('Curl error: [' . curl_errno($ch) . '] ' . curl_error($ch));
        }

        return $data;
    }
}