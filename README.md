# ExchangeRate is PHP test task

### Installation

```sh
git clone https://OlegKochetkov999@bitbucket.org/OlegKochetkov999/exchangeratetest.git
cd phptest/
composer install
composer dump-autoload
```
### Tests
```sh
vendor/bin/phpunit tests/
```

### Using
A simple way to use it is create a little cli command and run as `php cli.php`
```sh
<?php

require_once __DIR__ . '/vendor/autoload.php';

$settings = new \OK\ExchangeRate\Entity\ParameterBag(['currencyFrom' => 'USD', 'currencyTo' => 'RUR', 'date' => new \DateTime()]);

$params = (new \OK\ExchangeRate\ExchangeRate([new OK\ExchangeRate\Resource\Cbr(), new OK\ExchangeRate\Resource\Rbs()], $settings))->get();

var_dump($params);
```


