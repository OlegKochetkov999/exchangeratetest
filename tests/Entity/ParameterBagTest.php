<?php

namespace Tests;

use Tests\TestCase;
use OK\ExchangeRate\Entity\ParameterBag;

/**
 * @author Oleg Kochetkov <oleg.kochetkov999@yandex.ru>
 */
class ParameterBagTest extends TestCase
{
    public function testConstructor()
    {
        $date = new \DateTime();
        
        $data = [
            'currencyFrom' => 'USD',
            'currencyTo' => 'RUR',
            'date' => $date,
            'non_existing_property' => 'test'
        ];
        
        $bag = new ParameterBag($data);
        
        $this->assertEquals($data['currencyFrom'], $bag->getCurrencyFrom());
        $this->assertEquals($data['currencyTo'], $bag->getCurrencyTo());
        $this->assertEquals($data['date'], $bag->getDate());
    }
}