<?php

namespace Tests;

use Tests\TestCase;
use OK\ExchangeRate\Resource\Cbr;
use OK\ExchangeRate\Entity\ParameterBag;

/**
 * @author Oleg Kochetkov <oleg.kochetkov999@yandex.ru>
 */
class CbrTest extends TestCase
{
    public function testMapping()
    {
        $cbr = new Cbr();
        $date = new \DateTime('12.11.1989');
        $bag = $this->createMock(ParameterBag::class);
        $bag->method('getDate')->willReturn($date);
        
        $mapping = $this->makeCallable($cbr, 'mapping');
        $params = $this->makeCallableProperty($cbr, 'params');
        $mapping->invokeArgs($cbr, [$bag]);
        
        $this->assertEquals(['date_req' => $date->format('d/m/Y')], $params->getValue($cbr));
    }
}
