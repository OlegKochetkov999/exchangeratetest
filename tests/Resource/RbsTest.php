<?php

namespace Tests;

use Tests\TestCase;
use OK\ExchangeRate\Resource\Rbs;
use OK\ExchangeRate\Entity\ParameterBag;

/**
 * @author Oleg Kochetkov <oleg.kochetkov999@yandex.ru>
 */
class RbsTest extends TestCase
{
    public function testMapping()
    {
        $rbc = new Rbs();
        $date = new \DateTime('12.11.1989');
        $currencyFrom = 'USD';
        $currencyTo = 'RUR';

        $bag = $this->createMock(ParameterBag::class);
        $bag->method('getDate')->willReturn($date);
        $bag->method('getCurrencyFrom')->willReturn($currencyFrom);
        $bag->method('getCurrencyTo')->willReturn($currencyTo);
        
        $mapping = $this->makeCallable($rbc, 'mapping');
        $params = $this->makeCallableProperty($rbc, 'params');
        $mapping->invokeArgs($rbc, [$bag]);
        
        $this->assertEquals([
            'currency_from' => $currencyFrom,
            'currency_to' => $currencyTo,
            'source' => 'cbrf',
            'sum' => '1',
            'date' => $date->format('Y-m-d')
            ],
            $params->getValue($rbc)
        );
    }
}
