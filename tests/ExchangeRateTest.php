<?php

namespace Tests;

use Tests\TestCase;
use OK\ExchangeRate\ExchangeRate;
use OK\ExchangeRate\Resource\Cbr;
use OK\ExchangeRate\Resource\Rbs;
use OK\ExchangeRate\Exception\ExchangeException;

/** 
 * @author Oleg Kochetkov <oleg.kochetkov999@yandex.ru>
 */
class ExchangeRateTest extends TestCase
{
    /**
     * @dataProvider getProvider
     */
    public function testGet($value, $value2, $result)
    {
        $cbr = $this->createMock(Cbr::class);
        $cbr->method('get')->willReturn($value);
        
        $rbc = $this->createMock(Rbs::class);
        $cbr->method('get')->willReturn($value2);
        
        $exchange = new ExchangeRate([$cbr, $rbc]);

        if ($result === null) {
            $this->expectException(ExchangeException::class);
        }
        
        $valueResult = $exchange->get();
        
        if ($valueResult !== null) {
            $this->assertEquals($result, $valueResult);
        }
    }

    public function getProvider()
    {
        return [
            [60.55, 60.35, 60.4],
            [60.5, null, null],
        ];
    }
}

